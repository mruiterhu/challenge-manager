package nl.teamrepo.challengemanager.data;

import nl.teamrepo.challengemanager.domain.Challenge;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

class ChallengeDataParserTest {

    @Test
    void createChallengeJSONArrayConvertsToArrayListCorrectly() {
        JSONArray jsonArray = new JSONArray();

        JSONObject challengeJson = new JSONObject();
        challengeJson.put("id", 1L);
        challengeJson.put("name", "salad");
        challengeJson.put("description", "A certain Roman ");
        challengeJson.put("max_attempts", 4);
        challengeJson.put("value", 100);
        challengeJson.put("category", "cypher");
        challengeJson.put("type", "standard");
        challengeJson.put("state", "none");
        challengeJson.put("requirements", null);
        challengeJson.put("connectionInfo", null);

        jsonArray.add(challengeJson);

        ArrayList<Challenge> challenges = ChallengeDataParser.createChallengeFromJSONArray(jsonArray);
        assertEquals(jsonArray.size(), challenges.size());
    }
}