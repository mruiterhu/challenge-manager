import {css, html, LitElement} from 'https://unpkg.com/lit-element/lit-element.js?module';

class DataviewComponent extends LitElement {

    static get properties() {
        return {
            filteredList: {type: Array},
            exportChallengeList: {type: Array}
        }
    }

    static get styles() {
        return css`
          .data-view-container {
            display: flex;
            flex-direction: row;
            flex-wrap: wrap;
            justify-content: center;
            align-content: center;
            background-color: #01102c;
            margin: 0 13rem;
            padding-top: 1rem;
            border-radius: 10px;
          }
        `;
    }

    render() {
        if (this.filteredList === null || this.filteredList === undefined) return html``;

        return html`
            <div class="data-view-container">
                ${this.filteredList.map(challenge => {
                    for (let exportChallenge of this.exportChallengeList) {
                        if (exportChallenge === challenge) {
                            return html`<challenge-component .isChecked="${true}" .challengeData="${challenge}"></challenge-component>`;
                        }
                    }
                    
                    return html`<challenge-component .isChecked="${false}" .challengeData="${challenge}"></challenge-component>`;
                })}
            </div>
        `;
    }
}

customElements.define('data-view', DataviewComponent);
