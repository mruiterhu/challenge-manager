package nl.teamrepo.challengemanager.application;

import nl.teamrepo.challengemanager.data.ChallengeDataParser;
import nl.teamrepo.challengemanager.domain.Challenge;
import nl.teamrepo.challengemanager.domain.exceptions.ExportParseException;
import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipFile;

@Service
public class ChallengeService {

    public ChallengeService() {}

    /** This method can be used to get challenges from export files other than the currentCTFDExportZip. */
    public List<Challenge> getChallengesFromCTFDExport(ZipFile CTFDExportZip) throws IOException, ParseException, ExportParseException {
        return ChallengeDataParser.extractCTFDExportZipToChallengeList(CTFDExportZip);
    }

    public byte[] createNewExportFromChallenges(ZipFile CTFDExport, JSONObject challenges, boolean deletePersonalData) throws IOException {
        if (challenges == null || challenges.get("results").toString().equals("[]") || CTFDExport == null) return null;

        File newCTFDExportZip = File.createTempFile("UpdatedCTFDExport", null);
        Path currentExportZipPath = Paths.get(CTFDExport.getName());

        // Copy the current CTFD export zip to the new temporary one.
        FileUtils.copyFile(currentExportZipPath.toFile(),
                newCTFDExportZip);

        // Create a new challenges.json file, and write all new challenges to it.
        File updatedChallengesJSONFile = File.createTempFile("challengeJSON", null);
        Path updatedChallengesJSONPath = Paths.get(updatedChallengesJSONFile.getAbsolutePath());

        try (FileWriter fileWriter = new FileWriter(updatedChallengesJSONFile)) {
            fileWriter.write(challenges.toJSONString());
        } catch (Exception e ){
            e.printStackTrace();
        }

        // Replace the old challenges.json file with the new one.
        Path pathOfZipToEdit = Path.of(newCTFDExportZip.getAbsolutePath());
        try( FileSystem fs = FileSystems.newFileSystem(pathOfZipToEdit, (ClassLoader) null) ){
            Path oldChallengesJSONPath = fs.getPath("/db/challenges.json");
            Files.copy(updatedChallengesJSONPath, oldChallengesJSONPath, StandardCopyOption.REPLACE_EXISTING);

            // If necessary, remove personal data and all files attached to it.
            if (deletePersonalData) {
                List<String> filesToEmpty = new ArrayList<>(List.of("container_logsolves", "standard_log", "submissions",
                        "teams", "tracking", "unlocks"));
                replacePersonalDataFiles(fs, filesToEmpty);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        FileInputStream fis = new FileInputStream(newCTFDExportZip);
        byte[] fileInputByteArray = fis.readAllBytes();

        // Close streams and delete temporary files.
        fis.close();
        Files.delete(Path.of(newCTFDExportZip.getAbsolutePath()));
        Files.delete(Path.of(updatedChallengesJSONPath.toString()));

        return fileInputByteArray;
    }

    private void replacePersonalDataFiles(FileSystem fs, List<String> filesToEmpty) {
        try {
            File emptyFile = File.createTempFile("emptyJSON", null);
            Path pathOfEmptyFile = Paths.get(emptyFile.getAbsolutePath());

            for (String fileName: filesToEmpty) {
                Path fileToEditPath = fs.getPath("/db/" + fileName + ".json");
                Files.copy(pathOfEmptyFile, fileToEditPath, StandardCopyOption.REPLACE_EXISTING);
            }

            // Create a new users.json file, and write a standard admin user to it.
            File updatedUsersJSONFile = File.createTempFile("newUserJson", null);
            Path updatedUsersJSONPath = Paths.get(updatedUsersJSONFile.getAbsolutePath());

            FileWriter fileWriter = new FileWriter(updatedUsersJSONFile);
            fileWriter.write("{\"id\":1,\"oauth_id\":null,\"name\":\"admin\",\"password\":\"$bcrypt-sha256$2b,12$AvE1xNvOlS7NYqUb46W60u$X2vaT/Thb8JHJbTDCK/G/EthBg6T5RK\",\"email\":\"admin@mail.com\",\"type\":\"admin\",\"secret\":null,\"website\":null,\"affiliation\":null,\"country\":null,\"bracket\":null,\"hidden\":0,\"banned\":0,\"verified\":0,\"team_id\":null,\"created\":\"2022-06-27T12:04:11\"}");
            fileWriter.close();

            Path oldUsersJSONPath = fs.getPath("/db/users.json");
            Files.copy(updatedUsersJSONPath, oldUsersJSONPath, StandardCopyOption.REPLACE_EXISTING);

            // Remove temp files.
            Files.delete(Path.of(updatedUsersJSONPath.toString()));
            Files.delete(Path.of(pathOfEmptyFile.toString()));
        } catch (IOException e){
            e.printStackTrace();
        }
    }
}
