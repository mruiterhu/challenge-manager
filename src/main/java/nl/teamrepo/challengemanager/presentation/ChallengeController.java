package nl.teamrepo.challengemanager.presentation;

import nl.teamrepo.challengemanager.application.ChallengeService;
import nl.teamrepo.challengemanager.domain.Challenge;
import nl.teamrepo.challengemanager.domain.exceptions.ExportParseException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.server.ResponseStatusException;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipFile;

@RestController
@RequestMapping("/ctfdexport")
public class ChallengeController {
    private final ChallengeService challengeService;

    public ChallengeController(ChallengeService challengeService) {
        this.challengeService = challengeService;
    }

    @Bean(name = "multipartResolver")
    public CommonsMultipartResolver multipartResolver() {
        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setMaxUploadSize(100_000_000);
        return multipartResolver;
    }

    @CrossOrigin()
    @PostMapping("/upload")
    public List<Challenge> handleFileUpload(@RequestParam("file") MultipartFile file) {
        try {
            File tempFile = File.createTempFile("upload", null);
            file.transferTo(tempFile);
            ZipFile zipFile = new ZipFile(tempFile);

            List<Challenge> challenges = challengeService.getChallengesFromCTFDExport(zipFile);

            tempFile.deleteOnExit();
            return challenges;
        } catch (IOException | ParseException | ExportParseException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @CrossOrigin
    @PostMapping(value="/export", produces="application/zip")
    public byte[] export(@RequestParam("file") MultipartFile file, @RequestParam("jsonChallenges") String challengesJSON,
                         @RequestParam("deletePersonalData") boolean deletePersonalData) throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        JSONObject challenges = (JSONObject) parser.parse(challengesJSON);

        File tempFile = File.createTempFile("export", null);
        file.transferTo(tempFile);
        ZipFile zipFile = new ZipFile(tempFile);

        byte[] fileInputByteArray = challengeService.createNewExportFromChallenges(zipFile, challenges, deletePersonalData);

        if (fileInputByteArray == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Incorrect data.");
        }

        return fileInputByteArray;
    }
}
