package nl.teamrepo.challengemanager.domain;

import lombok.Getter;
import lombok.Setter;
import nl.teamrepo.challengemanager.domain.enums.ChallengeState;

import java.util.Locale;

@Getter
@Setter
public class Challenge {
    private long id;
    private String name;
    private String description;
    private int max_attempts;
    private int value;
    private String category;
    private String type;
    private ChallengeState state;
    private String requirements;
    private String connectionInfo;

    public Challenge(long id, String name, String description, int max_attempts, int value, String category, String type, ChallengeState state, String requirements, String connectionInfo) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.max_attempts = max_attempts;
        this.value = value;
        this.category = category;
        this.type = type;
        this.state = state;
        this.requirements = requirements;
        this.connectionInfo = connectionInfo;
    }

    @Override
    public String toString() {
        return "Id: " + 1 + ", name: " + name + "\n" +
                "Description: \n-------\n" + description + "\n---------" +
                "\nMax attempts: " + max_attempts + "\nValue: " + value +
                "\nCategory: " + category + "\nType: " + type +
                "\nState: " + state.toString().toLowerCase(Locale.ROOT) +
                "\nRequirements: " + requirements +
                "\nConnection info: " + connectionInfo;
    }
}
