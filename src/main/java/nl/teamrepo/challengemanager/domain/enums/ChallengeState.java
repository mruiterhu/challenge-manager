package nl.teamrepo.challengemanager.domain.enums;

public enum ChallengeState {
    VISIBLE,
    HIDDEN
}
